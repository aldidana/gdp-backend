# GDP Backend
API for GDP Backend

### Prerequisites
- Node.js minimum v6.0 - Download and Install [Node.js](https://nodejs.org/en/) with [NVM](https://github.com/creationix/nvm) (Node Version Manager) - Simple bash script to manage multiple active node.js versions.
- MongoDB - Download and Install [MongoDB](http://www.mongodb.org/) - Make sure it's running on the default port.
  - all Mongodb models its using [Mongoose](https://www.npmjs.org/package/mongoose) and all the file its on repo

- ```npm install```
- ```mv .env.sample .env```
- ```npm test``` for testing
- ```npm start``` for running the API
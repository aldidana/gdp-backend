const Api = require('../config/server')
const DB = Api.db
const { pricingRules } = require('../fixtures')

const ProductTest = require('./product.test')
const PricingRulesTest = require('./pricing-rules.test')
const DefaultTest = require('./default.test')
const UnileverTest = require('./unilever.test')
const AppleTest = require('./apple.test')
const NikeTest = require('./nike.test')
const FordTest = require('./ford.test')

const supertest = require('supertest')
const chai  = require('chai')
const expect  = chai.expect
chai.should()

const server = supertest(Api.server)

const Product = DB.model('Product')
const PricingRule = DB.model('PricingRule')
const Checkout = DB.model('Checkout')

ProductTest({
  server: server,
  product: Product,
  pricingRule: PricingRule,
  checkout: Checkout,
  expect: expect
})

PricingRulesTest({
  server: server,
  pricingRule: PricingRule,
  expect: expect,
  pricingRulesFixtures: pricingRules
})

DefaultTest({
  server: server,
  product: Product,
  expect: expect
})

UnileverTest({
  server: server,
  product: Product,
  expect: expect
})

AppleTest({
  server: server,
  product: Product,
  expect: expect
})

NikeTest({
  server: server,
  product: Product,
  expect: expect
})

FordTest({
  server: server,
  product: Product,
  expect: expect
})

after((done) => {
  Api.server.close()
  done()
})

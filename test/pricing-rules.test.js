module.exports = options => {
  const server = options.server
  const expect = options.expect
  const pricingRulesFixtures = options.pricingRulesFixtures
  const PricingRule = options.pricingRule

  describe('Pricing Rules scenario', () => {
    before(done => {
      PricingRule.remove({}).exec(() => {
        PricingRule.create(pricingRulesFixtures, (err, rules) => {
          done()
        })
      })
    })

    it('GET /v1/pricing-rules should return all pricing rules with total 6 rules', (done) => {
      server
        .get('/v1/pricing-rules')
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)
          res.body.should.have.lengthOf(6)
          done()
        })
    })

    it('POST /v1/pricing-rules create new pricing rules and should return one new pricing-rules', (done) => {
      const newPricingRules = {
        name: 'GDP for 5 for 4',
        customer: 'GDP',
        product_id: 'premium',
        get_bonus_minimum: 4,
        get_bonus_free: 1,
        discount_minimum: null,
        discount_sale_price: null
      }

      server
        .post('/v1/pricing-rules')
        .send(newPricingRules)
        .expect('Content-type', /json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('name')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('get_bonus')
          expect(res.body.data).to.have.property('discount')

          res.body.data.name.should.equal(newPricingRules.name)
          res.body.data.customer.should.equal(newPricingRules.customer)
          res.body.data.product_id.should.equal(newPricingRules.product_id)
          res.body.data.get_bonus.minimum.should.equal(newPricingRules.get_bonus_minimum)
          res.body.data.get_bonus.free.should.equal(newPricingRules.get_bonus_free)
          expect(res.body.data.discount.minimum).to.be.null
          expect(res.body.data.discount.sale_price).to.be.null

          done()
        })
    })

    it('GET /v1/pricing-rules should return all pricing rules with total 7 rules', (done) => {
      server
        .get('/v1/pricing-rules')
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)
          res.body.should.have.lengthOf(7)
          done()
        })
    })
  })
}

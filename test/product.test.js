module.exports = options => {
  const server = options.server
  const expect = options.expect
  const Product = options.product
  const Checkout = options.checkout
  const PricingRule = options.pricingRule

  describe('Product scenario', () => {
    before(done => {
      Product.remove({}).exec(() => {
        Product
          .create([
            {
              product_id: 'classic',
              name: 'Classic Ad',
              price: 269.99
            }, {
              product_id: 'standout',
              name: 'Standout Ad',
              price: 322.99
            }], (err, docs) => {
              if (err) console.error(err)

              docs.map(prod => {
                global['product_' + prod.product_id] = prod
              })

              Checkout.remove({}).exec()
              PricingRule.remove({}).exec()

              done()
            })
      })
  })

  it('GET /v1/product should return all product with total 2 products', (done) => {
    server
      .get('/v1/product')
      .expect('Content-type',/json/)
      .expect(200)
      .end((err, res) => {
        res.status.should.equal(200)
        res.body.should.have.lengthOf(2)
        done()
      })
  })

  it('POST /v1/product create new product and should return one new product', (done) => {
    const newProduct = {
      product_id: 'premium',
      name: 'Premium Ad',
      price: 394.99
    }

    server
      .post('/v1/product')
      .send(newProduct)
      .expect('Content-type', /json/)
      .expect(200)
      .end((err, res) => {
        res.status.should.equal(200)

        expect(res.body).to.have.property('product_id')
        expect(res.body).to.have.property('name')
        expect(res.body).to.have.property('price')

        res.body.product_id.should.equal(newProduct.product_id)
        res.body.name.should.equal(newProduct.name)
        res.body.price.should.equal(newProduct.price)

        global[`product_${res.body.product_id}`] = res.body

        done()
      })
  })

  it('GET /v1/product/:productId get one product and should return one product', (done) => {
    const queryProduct = product_classic.product_id

    server
      .get(`/v1/product/${queryProduct}`)
      .expect('Content-type',/json/)
      .expect(200)
      .end((err, res) => {
        res.status.should.equal(200)

        expect(res.body).to.have.property('product_id')
        expect(res.body).to.have.property('name')
        expect(res.body).to.have.property('price')

        res.body.product_id.should.equal(product_classic.product_id)
        res.body.name.should.equal(product_classic.name)
        res.body.price.should.equal(product_classic.price)

        done()
      })
  })

  it('PUT /v1/product/:productId update one product and should return one product', (done) => {
    const queryProduct = product_classic._id
    const updateProduct = {
      product_id: product_classic.product_id,
      name: 'Classic Advertise',
      price: 100
    }

    server
      .put(`/v1/product/${queryProduct}`)
      .send(updateProduct)
      .expect('Content-type',/json/)
      .expect(200)
      .end((err, res) => {
        res.status.should.equal(200)

        expect(res.body).to.have.property('product_id')
        expect(res.body).to.have.property('name')
        expect(res.body).to.have.property('price')

        res.body.product_id.should.equal(updateProduct.product_id)
        res.body.name.should.equal(updateProduct.name)
        res.body.price.should.equal(updateProduct.price)
        done()
      })
  })

  it('DELETE /v1/product/:productId delete one product and should return one product', (done) => {
    const queryProduct = product_classic._id

    server
      .delete(`/v1/product/${queryProduct}`)
      .expect('Content-type',/json/)
      .expect(200)
      .end((err, res) => {
        res.status.should.equal(200)
        done()
      })
  })

  it('POST /v1/product create new product and should return one new product', (done) => {
    const newProduct = {
      product_id: 'classic',
      name: 'Classic Ad',
      price: 269.99
    }

    server
      .post('/v1/product')
      .send(newProduct)
      .expect('Content-type', /json/)
      .expect(200)
      .end((err, res) => {
        res.status.should.equal(200)

        expect(res.body).to.have.property('product_id')
        expect(res.body).to.have.property('name')
        expect(res.body).to.have.property('price')

        res.body.product_id.should.equal(newProduct.product_id)
        res.body.name.should.equal(newProduct.name)
        res.body.price.should.equal(newProduct.price)

        global[`product_${res.body.product_id}`] = res.body

        done()
      })
  })
})
}

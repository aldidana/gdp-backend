module.exports = options => {
  const server = options.server
  const Product = options.product
  const expect = options.expect

  describe('Ford customer checkout scenario', () => {
    before(done => {
      Product
        .find({})
        .lean()
        .exec((err, products) => {
          if (err) console.error(err)

          products.map(prod => {
            global['product_' + prod.product_id] = prod
          })
          done()
        })
    })

    let expectedCheckout = {
      total: 2869.91,
      items: ['classic', 'classic', 'classic', 'classic', 'classic', 'standout', 'premium', 'premium', 'premium'],
      customer: 'FORD'
    }

    it('POST /v1/checkout 1st checkout classic ad should return checkout classic ad item', (done) => {
      const checkoutItem = {
        product_id: product_classic.product_id,
        customer: 'Ford'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_classic.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_classic.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 2nd checkout classic ad should return checkout classic ad item', (done) => {
      const checkoutItem = {
        product_id: product_classic.product_id,
        customer: 'Ford'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_classic.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_classic.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 3rd checkout classic ad should return checkout classic ad item', (done) => {
      const checkoutItem = {
        product_id: product_classic.product_id,
        customer: 'Ford'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_classic.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_classic.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 4th checkout classic ad should return checkout classic ad item', (done) => {
      const checkoutItem = {
        product_id: product_classic.product_id,
        customer: 'Ford'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_classic.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_classic.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 1st checkout standout ad should return checkout standout ad item', (done) => {
      const checkoutItem = {
        product_id: product_standout.product_id,
        customer: 'Ford'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_standout.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_standout.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 2nd checkout standout ad should return checkout standout ad item', (done) => {
      const checkoutItem = {
        product_id: product_standout.product_id,
        customer: 'Ford'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_standout.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_standout.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 1st checkout premium ad should return checkout premium ad item', (done) => {
      const checkoutItem = {
        product_id: product_premium.product_id,
        customer: 'FORD'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_premium.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_premium.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 2nd checkout premium ad should return checkout premium ad item', (done) => {
      const checkoutItem = {
        product_id: product_premium.product_id,
        customer: 'FORD'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_premium.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_premium.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('POST /v1/checkout 3rd checkout premium ad should return checkout premium ad item', (done) => {
      const checkoutItem = {
        product_id: product_premium.product_id,
        customer: 'FORD'
      }

      server
        .post('/v1/checkout')
        .send(checkoutItem)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expect(res.body.data).to.have.property('product_id')
          expect(res.body.data).to.have.property('customer')
          expect(res.body.data).to.have.property('price')

          res.body.data.product_id.should.equal(product_premium.product_id)
          res.body.data.customer.should.equal('FORD')
          res.body.data.price.should.equal(product_premium.price)

          expect(expectedCheckout.customer).to.equal(res.body.data.customer)

          done()
        })
    })

    it('GET /v1/checkout/:customer get total checkout for Ford', (done) => {
      server
        .get(`/v1/checkout/ford`)
        .expect('Content-type',/json/)
        .expect(200)
        .end((err, res) => {
          res.status.should.equal(200)

          expectedCheckout.total.should.equal(res.body.total)

          expect(res.body.items).to.deep.include.members(expectedCheckout.items)

          done()
        })
    })
  })
}

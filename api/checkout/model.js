const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')
const Schema = mongoose.Schema

const CheckoutSchema = new Schema({
  product_id: {
    type: String,
    required: true,
    lowercase: true
  },
  customer: {
    type: String,
    required: true,
    uppercase: true
  },
  price: {
    type: Number,
    required: true,
    default: 0
  },
  is_free_item: {
    type: Boolean,
    default: false
  }
})

CheckoutSchema.plugin(timestamps)

module.exports = mongoose.model('Checkout', CheckoutSchema)

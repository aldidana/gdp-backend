const PricingRules = require('../pricing-rules/model')
const ProductService = require('../product/services')
const Checkout = require('./model')
const async = require('async')
const _ = require('lodash')

function GetPricingRules (body, next) {
  PricingRules
    .findOne({customer: body.customer.toUpperCase(), product_id: body.product_id.toLowerCase()})
    .lean()
    .exec((err, rules) => next(err, rules))
}

function CheckoutProduct(rules) {
  this.Add = (item, next) => {
    const checkoutQuery = { customer: item.customer.toUpperCase(), product_id: item.product_id, is_free_item: false }
    async.waterfall([
      addOneItem,
      countCheckoutItems
    ], (err, finish) => {
      next(err, finish)
    })

    function addOneItem (cb) {
      ProductService.GetProduct(item.product_id, (err, itemDetail) => {
        if (err) return cb(err, itemDetail)

        const addItem = new Checkout({
          product_id: item.product_id,
          customer: item.customer,
          price: itemDetail.price
        })

        addItem.save((err, addedItem) => {
          return cb(err, addedItem)
        })
      })
    }

    function countCheckoutItems (addedItem, cb) {
      Checkout
        .count(checkoutQuery)
        .exec((err, checkoutCount) => {
          if (checkoutCount > 1 && rules && rules.get_bonus && (checkoutCount === rules.get_bonus.minimum || (checkoutCount % rules.get_bonus.minimum) === 0)) {
            const addBonus = new Checkout({
              product_id: item.product_id,
              customer: item.customer.toUpperCase(),
              price: 0,
              is_free_item: true
            })

            addBonus.save((err, bonusItem) => {
              cb(err, addedItem)
            })
          } else if (rules && rules.discount.minimum && ( checkoutCount >= rules.discount.minimum )) {
            Checkout
              .update(checkoutQuery, { $set: { price: rules.discount.sale_price } }, { multi: true })
              .lean()
              .exec((err, updatedItem) => {
                cb(err, addedItem)
              })
          } else {
            cb(err, addedItem)
          }
        })
    }
  }
}

function TotalCheckout(customer, next) {
  Checkout
    .find({customer: customer.toUpperCase()})
    .lean()
    .exec((err, checkouts) => {
      if (err) return next(err, checkouts)

      if (checkouts) {
        const allCheckout = {
          product: [],
          total: 0,
          items: []
        }

        _.map(checkouts, item => {
          allCheckout.product.push(item)
          allCheckout.total += item.price
          allCheckout.items.push(item.product_id)
        })

        next(err, allCheckout)
      } else {
        next(null, null)
      }
    })
}

module.exports = {
  GetPricingRules: GetPricingRules,
  CheckoutProduct: CheckoutProduct,
  TotalCheckout: TotalCheckout
}

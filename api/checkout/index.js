const checkout = require('./checkout')

module.exports = {
  totalCheckout: checkout.totalCheckout,
  create: checkout.create,
}

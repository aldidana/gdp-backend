const Services = require('./services')

const checkoutHandler = {}

/**
 * GET /v1/checkout/:customer
 *
 * @desc Get checkout for one customer
 *
 * @param {object} req - Parameters for request
 * @param {objectId} req.params.customer - customer name
 * @param {object} res - Response Object
 * @return {object} json - One Product
 */
checkoutHandler.totalCheckout = (req, res) => {
  req.checkParams('customer', 'Invalid product id').notEmpty()

  if (req.validationErrors()) {
    return res.status(400).json({error: req.validationErrors(true)})
  }

  Services.TotalCheckout(req.params.customer.toUpperCase(), (err, checkouts) => {
    if (err) return res.status(400).send({error: err})

    if (!checkouts) return res.status(404).send({error: `You haven't add an item`})

    return res.status(200).json(checkouts)
  })
}

/**
 * POST /v1/checkout/
 *
 * @desc Checkout
 *
 * @param  {object} req - Parameters for request
 * @param  {objectId} req.body.product_id - Product identifier
 * @param  {string} req.body.customer - Customer name
 * @param {object} res - Response Object
 * @return {object} json - New Product
 */
checkoutHandler.create = (req, res) => {
  req.checkBody('product_id', 'Invalid product_id').notEmpty()
  req.checkBody('customer', 'Invalid customer').notEmpty()

  if (req.validationErrors()) {
    return res.status(400).json({error: req.validationErrors(true)})
  }

  Services.GetPricingRules(req.body, (err, rules) => {
    if (err) return res.status(400).send({error: err})

    const checkoutProduct = new Services.CheckoutProduct(rules)
    checkoutProduct.Add(req.body, (err, checkoutItem) => {
      if (err) return res.status(400).send({error: err})

      return res.status(200).json({message: 'Success added item', data: checkoutItem})
    })
  })
}

module.exports = checkoutHandler

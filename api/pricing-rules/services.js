const PricingRules = require('./model')

module.exports = {
  AllRules: (next) => {
    PricingRules
      .find({})
      .lean()
      .exec((err, rules) => next(err, rules))
  },
  CreateRules: (data, next) => {
    const createRule = new PricingRules({
      name: data.name,
      customer: data.customer,
      product_id: data.product_id,
      get_bonus: {
        minimum: data.get_bonus_minimum,
        free: data.get_bonus_free
      },
      discount: {
        minimum: data.discount_minimum,
        sale_price: data.discount_sale_price
      }
    })

    createRule.save((err, newRule) => next(err, newRule))
  }
}

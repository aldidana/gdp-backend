const Services = require('./services')

const pricingRulesHandler = {}

/**
 * GET /v1/pricing-rules/
 *
 * @desc Get list of all pricing rules
 *
 * @param {Request} request Request object
 * @param {Response} response Response object
 * @return {object} json - All pricing rules
 */
pricingRulesHandler.index = (req, res) => {
  Services.AllRules((err, rules) => {
    if (err) return res.status(400).json({error: err})

    return res.status(200).json(rules)
  })
}

/**
 * POST /v1/pricing-rules/
 *
 * @desc Create new pricing rule
 *
 * @param  {object} req - Parameters for request
 * @param  {objectId} req.body.product_id - Product identifier
 * @param  {string} req.body.customer - Customer name
 * @param  {string} req.body.product_id - Product_id name
 * @param  {string} req.body.get_bonus_minimum - Minimum to get bonus
 * @param  {string} req.body.get_bonus_free - Bonus item
 * @param  {string} req.body.discount_minimum - Minimum to get discount
 * @param  {string} req.body.discount_sale_price - Price when sale
 * @param {object} res - Response Object
 * @return {object} json - New Product
 */
pricingRulesHandler.create = (req, res) => {
  req.checkBody('name', 'Invalid pricing rule name').notEmpty()
  req.checkBody('customer', 'Invalid customer name').notEmpty()
  req.checkBody('product_id', 'Invalid product_id').notEmpty()

  if (req.validationErrors()) {
    return res.status(400).json({error: req.validationErrors(true)})
  }

  Services.CreateRules(req.body, (err, rules) => {
    if (err) return res.status(400).send({error: err})

    return res.status(200).json({message: 'Success create new pricing rules', data: rules})
  })
}

module.exports = pricingRulesHandler

const pricingRules = require('./pricing-rules')

module.exports = {
  index: pricingRules.index,
  create: pricingRules.create
}

const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')
const Schema = mongoose.Schema

const PricingRuleSchema = new Schema({
  name: {
    type: String
  },
  customer: {
    type: String,
    required: true
  },
  product_id: {
    type: String,
    required: true
  },
  get_bonus: {
    minimum: Number,
    free: Number
  },
  discount: {
    minimum: Number,
    sale_price: Number
  }
})

PricingRuleSchema.plugin(timestamps)

module.exports = mongoose.model('PricingRule', PricingRuleSchema)

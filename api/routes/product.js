'use strict'

const express = require('express')
const Route = express.Router()
const ProductHandler = require('../product')

Route
  .get('/', ProductHandler.index)
  .get('/:productId', ProductHandler.getProduct)
  .post('/', ProductHandler.create)
  .put('/:productId', ProductHandler.update)
  .delete('/:productId', ProductHandler.delete)

module.exports = Route

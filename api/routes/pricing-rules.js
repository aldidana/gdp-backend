'use strict'

const express = require('express')
const Route = express.Router()
const PricingRulesHandler = require('../pricing-rules')

Route
  .get('/', PricingRulesHandler.index)
  .post('/', PricingRulesHandler.create)

module.exports = Route

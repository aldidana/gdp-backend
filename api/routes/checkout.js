'use strict'

const express = require('express')
const Route = express.Router()
const CheckoutHandler = require('../checkout')

Route
  .get('/:customer', CheckoutHandler.totalCheckout)
  .post('/', CheckoutHandler.create)

module.exports = Route

'use strict'

const fs = require('fs')
const path = require('path')
const ROOT_DIR = process.cwd()

module.exports = app => {
  fs.readdirSync(ROOT_DIR + '/api/routes').forEach(file => {
    const extname = path.extname(file)
    const basename = path.basename(file, extname)

    if (~file.indexOf('.js') && basename !== 'index') {
      app.use(`/v1/${basename}`, require(ROOT_DIR + '/api/routes/' + file))
    }
  })
}

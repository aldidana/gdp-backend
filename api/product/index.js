const product = require('./product')

module.exports = {
  index: product.index,
  getProduct: product.getProduct,
  create: product.create,
  delete: product.delete,
  update: product.update
}

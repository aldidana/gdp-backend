const mongoose = require('mongoose')
const timestamps = require('mongoose-timestamp')
const Schema = mongoose.Schema

const ProductSchema = new Schema({
  product_id: {
    type: String,
    lowercase: true,
    unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  }
})

ProductSchema.plugin(timestamps)

module.exports = mongoose.model('Product', ProductSchema)

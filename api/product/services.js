const Product = require('./model')

module.exports = {
  ListAllProduct: (next) => {
    Product
      .find({})
      .lean()
      .exec((err, doc) => next(err, doc))
  },
  GetProduct: (productId, next) => {
    Product
      .findOne({product_id: productId})
      .lean()
      .exec((err, doc) => next(err, doc))
  },
  CreateProduct: (data, next) => {
    const createProduct = new Product({
      product_id: data.product_id,
      name: data.name,
      price: data.price
    })

    createProduct.save((err, newProduct) => next(err, newProduct))
  },
  UpdateProduct: (productId, data, next) => {
    Product
      .findByIdAndUpdate(productId, { $set: data }, { new: true })
      .lean()
      .exec((err, product) => next(err, product))
  },
  DeleteProduct: (productId, next) => {
    Product
      .remove({ _id: productId })
      .lean()
      .exec((err, info) => next(err, info))
  }
}

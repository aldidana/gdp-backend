const Services = require('./services')

const ProductHandler = {}

/**
 * GET /v1/product/
 *
 * @desc Get list of all products
 *
 * @param {Request} request Request object
 * @param {Response} response Response object
 * @return {object} json - All product
 */
ProductHandler.index = (req, res) => {
  Services.ListAllProduct((err, products) => {
    if (err) return res.status(400).json({error: err})

    return res.status(200).json(products)
  })
}

/**
 * GET /v1/product/:productId
 *
 * @desc Get one product detail
 *
 * @param {object} req - Parameters for request
 * @param {objectId} req.params.productId - Product _id from mongo id
 * @param {object} res - Response Object
 * @return {object} json - One Product
 */
ProductHandler.getProduct = (req, res) => {
  req.checkParams('productId', 'Invalid product id').notEmpty()

  if (req.validationErrors()) {
    return res.status(400).json({error: req.validationErrors(true)})
  }

  Services.GetProduct(req.params.productId, (err, product) => {
    if (err) return res.status(400).send({error: err})

    if (!product) return res.status(404).send({error: 'Product not found'})

    return res.status(200).json(product)
  })
}

/**
 * POST /v1/product/
 *
 * @desc Create new product
 *
 * @param  {object} req - Parameters for request
 * @param  {objectId} req.body.product_id - Product identifier
 * @param  {string} req.body.name - Product name
 * @param  {number} req.body.price - Product price
 * @param {object} res - Response Object
 * @return {object} json - New Product
 */
ProductHandler.create = (req, res) => {
  req.checkBody('product_id', 'Invalid product_id').notEmpty()
  req.checkBody('name', 'Invalid name').notEmpty()
  req.checkBody('price', 'Invalid price').notEmpty().isFloat()

  if (req.validationErrors()) {
    return res.status(400).json({error: req.validationErrors(true)})
  }

  Services.CreateProduct(req.body, (err, newProduct) => {
    if (err) return res.status(400).send({error: err})

    return res.status(200).json(newProduct)
  })
}

/**
 * PUT /v1/product/:productId
 *
 * @desc Update one product
 *
 * @param  {object} req - Parameters for request
 * @param  {objectId} req.param.productId - Product _id from mongo id
 * @param  {objectId} req.body.product_id - Product identifier
 * @param  {string} req.body.name - Product name
 * @param  {number} req.body.price - Product price
 * @param {object} res - Response object
 * @return {object} json - Updated product
 */
ProductHandler.update = (req, res) => {
  req.checkBody('name', 'Invalid name').notEmpty()
  req.checkBody('price', 'Invalid price').notEmpty().isFloat()

  if (req.validationErrors()) {
    return res.status(400).json({error: req.validationErrors(true)})
  }

  const updateData = req.body

  Services.UpdateProduct(req.params.productId, updateData, (err, updatedData) => {
    if (err) return res.status(400).send({error: err})

    return res.status(200).json(updatedData)
  })
}

/**
 * DELETE /v1/product/:productId
 * @desc Delete one product
 *
 * @param {Request} request Request object
 * @param  {objectId} req.param.productId - Product _id from mongo id
 * @param {Response}response Response object
 * @return {object} json - Updated product
 */
ProductHandler.delete =  (req, res) => {
  req.checkParams('productId', 'Invalid product id').notEmpty()

  Services.DeleteProduct(req.params.productId, (err, info) => {
    if (err) return res.status(400).send({error: err})

    return res.status(200).json(info)
  })
}

module.exports = ProductHandler

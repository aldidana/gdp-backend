const products = [{
  product_id: 'classic',
  name: 'Classic Ad',
  price: 269.99
}, {
  product_id: 'standout',
  name: 'Standout Ad',
  price: 322.99
}, {
  product_id: 'premium',
  name: 'Premium Ad',
  price: 394.99
}]

const pricingRules = [
  {
    name: 'Unilever 3 for 2 classic',
    customer: 'UNILEVER',
    product_id: 'classic',
    get_bonus: {
      minimum: 2,
      free: 1
    },
    discount: {
      minimum: null,
      sale_price: null
    }
  },
  {
    name: 'Apple price drops standout',
    customer: 'APPLE',
    product_id: 'standout',
    get_bonus: {
      minimum: null,
      free: null
    },
    discount: {
      minimum: 1,
      sale_price: 299.99
    }
  },
  {
    name: 'Nike Price drops for min 4 premium purchase',
    customer: 'NIKE',
    product_id: 'premium',
    get_bonus: {
      minimum: null,
      free: null
    },
    discount: {
      minimum: 4,
      sale_price: 379.99
    }
  },
  {
    name: 'Ford 5 for 4 classic',
    customer: 'FORD',
    product_id: 'classic',
    get_bonus: {
      minimum: 4,
      free: 1
    },
    discount: {
      minimum: null,
      sale_price: null
    }
  },
  {
    name: 'Ford price drops standout',
    customer: 'FORD',
    product_id: 'standout',
    get_bonus: {
      minimum: null,
      free: null
    },
    discount: {
      minimum: 1,
      sale_price: 309.99
    }
  },
  {
    name: 'Ford price drops premium',
    customer: 'FORD',
    product_id: 'premium',
    get_bonus: {
      minimum: null,
      free: null
    },
    discount: {
      minimum: 3,
      sale_price: 389.99
    }
  }
]

module.exports.products = products
module.exports.pricingRules = pricingRules

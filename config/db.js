'use strict'

function connect (config) {
  const mongoose = require('mongoose')
  const options = { server: { socketOptions: { keepAlive: 1 } } }
  const async = require('async')
  const logger = require('winston')
  mongoose.Promise = global.Promise
  mongoose.connect(config.DB, options, err => {
    const conn = mongoose.connection

    const gracefulExit = () => {
      conn.close(function () {
        process.exit(0)
      })
    }

    //create fixtures for development
    if (config.NODE_ENV === 'development') {
      const Product = require('../api/product/model')
      const PricingRule = require('../api/pricing-rules/model')
      const fixtures = require('../fixtures')

      async.parallel([
        createProduct,
        createPricingRules
      ], (err, fixtures) => {
        if (err) console.error(err)

        logger.info(`Finish create fixtures`)
      })

      function createProduct(cb) {
        Product
          .count({}).exec((err, doc) => {
            if (err) console.error(err)
            if (doc < 1) {
              Product
                .create(fixtures.products, (err, product) => {
                  if (err) console.error(err)

                  cb(err, product)
                })
            } else {
              cb(err, doc)
            }
          })
      }
      function createPricingRules(cb) {
        PricingRule
          .count({})
          .exec((err, doc) => {
            if (err) console.error(err)
            if (doc < 1) {
                PricingRule
                  .create(fixtures.pricingRules, (err, rules) => {
                    if (err) console.error(err)

                    cb(err, rules)
                  })
            } else {
              cb(err, doc)
            }
          })
      }
    }

    // If the Node process ends, close the Mongoose connection
    process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit)
  })

  return mongoose
}

module.exports = connect

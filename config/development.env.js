'use strict'

const envArray = [
  'PORT',
  'NODE_ENV',
  'DB'
]

envArray.forEach(name => {
  if (!process.env[name]) {
    throw new Error(`Environment variable for ${name} is missing`)
  }
})

const config = {
  PORT: process.env.PORT,
  NODE_ENV: process.env.NODE_ENV,
  DB: process.env.DB
}

module.exports = config

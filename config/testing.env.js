'use strict'

const config = {
  PORT: 9090,
  NODE_ENV: 'testing',
  DB: 'mongodb://localhost:27017/gdp-test'
}

module.exports = config

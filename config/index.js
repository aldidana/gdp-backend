'use strict'

const path = require('path')
const util = require('util')
const extend = require('extend')

const dirConf = util.format('./%s.env.js', process.env.NODE_ENV || 'development')

module.exports = require(dirConf)

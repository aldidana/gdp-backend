const express = require('express')
const app = express()
const logger = require('winston')
const expressValidator = require('express-validator')()
const async = require('async')
const helmet = require('helmet')
const xssFilter = require('x-xss-protection')()
const bodyParser = require('body-parser')
const methodOverride = require('method-override')()
const http = require('http')
const config = require('./')

const PORT = config.PORT || 9090
const ROOT_DIR = process.cwd()
const ENV = config.NODE_ENV || 'development'

const listenServer = () => {
	http.createServer(app).listen(PORT, () => {
    if (config.NODE_ENV === 'development') {
      logger.info(`GDP Backend listening on port ${PORT}!`)
    }
	})
}
const parallelMiddleware = middlewares => (req, res, next) => async.each(middlewares, (mw, cb) => mw(req, res, cb), next)
const DB = require('./db')(config)

if (ENV === 'development') {
  const morgan = require('morgan')
  const responseTime = require('response-time')()
  const errorHandler = require('errorhandler')()

  app.use(morgan('dev'))
  app.use(responseTime)
  app.use(errorHandler)
}

//use 3rd party middlewares
app.use(parallelMiddleware([
	helmet.frameguard('sameorigin'),
	xssFilter,
	methodOverride,
	bodyParser.json({limit: '20mb'}),
  bodyParser.urlencoded({extended: true, limit: '20mb'}),
  expressValidator
]))

//preventing 'etag' 304
app.disable('etag')

require(`${ROOT_DIR}/api/routes`)(app)

DB.connection
	.on('error', logger.error)
	.on('disconnect', () => DB)
	.once('open', listenServer)

module.exports.server = http.createServer(app)
module.exports.db = DB
